// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_PlaerPawnBase_generated_h
#error "PlaerPawnBase.generated.h already included, missing '#pragma once' in PlaerPawnBase.h"
#endif
#define SNAKEGAME_PlaerPawnBase_generated_h

#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlaerPawnBase(); \
	friend struct Z_Construct_UClass_APlaerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlaerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlaerPawnBase)


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlaerPawnBase(); \
	friend struct Z_Construct_UClass_APlaerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlaerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlaerPawnBase)


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlaerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlaerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaerPawnBase(APlaerPawnBase&&); \
	NO_API APlaerPawnBase(const APlaerPawnBase&); \
public:


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaerPawnBase(APlaerPawnBase&&); \
	NO_API APlaerPawnBase(const APlaerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlaerPawnBase)


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_12_PROLOG
#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_INCLASS \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_PlaerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class APlaerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_PlaerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
